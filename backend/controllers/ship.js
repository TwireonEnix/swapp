const { Ship } = require('../models/ship');

module.exports = {
  createShip(req, res, next) {
    const shipProps = {
      name: req.body.name,
      model: req.body.model,
      speed: req.body.speed,
      hyperdriveRating: req.body.hyperdriveRating
    };
    Ship.create(shipProps).then(ship => res.status(201).send(ship)).catch(next);
  },

  editShip(req, res, next) {
    const shipProps = {
      name: req.body.name,
      model: req.body.model,
      speed: req.body.speed,
      hyperdriveRating: req.body.hyperdriveRating
    }
    Ship.findByIdAndUpdate({ _id: req.params.id }, shipProps, { new: true }).then(modifiedShip => {
      if (!modifiedShip) return res.status(404).send('Ship not found');
      res.send(modifiedShip);
    }).catch(next);
  },

  deleteShip(req, res, next) {
    Ship.findByIdAndRemove({ _id: req.params.id }).then(deletedShip => {
      if (!deletedShip) return res.status(404).send('Ship not found');
      res.send(deletedShip);
    }).catch(next);
  },

  getShips(req, res, next) {
    Ship.find().then(ships => res.send(ships)).catch(next);
  },

  getSingleShip(req, res, next) {
    Ship.findById(req.params.id).then(ship => {
      if (!ship) return res.status(404).send('Ship not found');
      res.send(ship);
    }).catch(next);
  }

};