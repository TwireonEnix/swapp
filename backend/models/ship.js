const mongoose = require('mongoose');
const Joi = require('joi');
Joi.objectId = require('joi-objectid');

const shipSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    minlength: 3,
    maxlength: 255
  },
  model: {
    type: String,
    required: true,
    minlength: 3,
    maxlength: 255
  },
  speed: {
    type: Number,
    required: true,
    min: 0
  },
  hyperdriveRating: {
    type: Number,
    required: true,
    min: 0
  },
})

const Ship = mongoose.model('ship', shipSchema);

const validateShip = ship => Joi.validate(ship, {
  name: Joi.string().min(3).max(255).required(),
  model: Joi.string().min(3).max(255).required(),
  speed: Joi.number().min(0).required(),
  hyperdriveRating: Joi.number().min(0).required(),
});

module.exports.Ship = Ship;
module.exports.validate = validateShip;