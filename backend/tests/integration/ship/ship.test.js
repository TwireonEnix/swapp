const { expect } = require('chai');
const request = require('supertest');
const { Ship } = require('../../../models/ship.js');
const { ObjectId } = require('mongoose').Types;
const app = require('../../../app');

describe('POST /api/ship', () => {
  let payload, shipId;
  const execute = () => request(app).post('/api/ship').send(payload);

  beforeEach(() => {
    payload = {
      name: 'exampleName',
      model: 'exampleModel',
      speed: 1,
      hyperdriveRating: 1
    };
  });

  afterEach(done => {
    Ship.deleteMany().then(() => done());
  });

  it('should return 400 if any property is not provided', done => {
    delete payload.speed;
    execute().expect(400).end(done);
  });

  it('should return 400 if name is less than 3 characters in length', done => {
    payload.name = 'ab';
    execute().expect(400).end(done);
  });

  it('should return 400 if speed is not a number', done => {
    payload.speed = true;
    execute().expect(400).end(done);
  })

  it('should create a ship in db if input is valid', done => {
    Ship.countDocuments().then(count => {
      execute().expect(201).end((err, res) => {
        Ship.countDocuments().then(newCount => {
          expect(newCount).to.equal(count + 1);
          return Ship.findOne({ name: payload.name });
        }).then(ship => {
          expect(ship).to.exist;
          expect(ship.name).to.equal(ship.name);
          expect(ship.speed).to.equal(ship.speed);
          done();
        });
      });
    });
  });

});

describe('PUT /api/ship/:id', () => {
  let payload, ship, shipId;

  const execute = () => request(app).put(`/api/ship/${shipId}`).send(payload);

  beforeEach(done => {
    shipId = new ObjectId();
    ship = {
      _id: shipId,
      name: 'exampleName',
      model: 'exampleModel',
      speed: 1,
      hyperdriveRating: 1
    };
    payload = {
      name: 'otherName',
      model: 'otherModel',
      speed: 2,
      hyperdriveRating: 2
    };
    Ship.create(ship).then(() => done());
  });

  afterEach(done => {
    Ship.deleteMany().then(() => done());
  });

  it('should return 400 if id is invalid', done => {
    shipId = null;
    execute().expect(400).end(done);
  });

  it('should return 400 if any input is missing', done => {
    delete payload.name;
    delete payload.speed;
    execute().expect(400).end(done);
  });

  it('should return 404 if ship with passed id is not found', done => {
    shipId = new ObjectId();
    execute().expect(404).end(done);
  });

  it('should update the ship', done => {
    execute().expect(200).end((err, res) => {
      expect(res.body.name).to.equal(payload.name);
      Ship.findById(shipId).then(shipInDb => {
        expect(shipInDb.name).to.equal(payload.name);
        expect(shipInDb.speed).to.equal(payload.speed);
        done();
      })
    });
  });


});

describe('DELETE /api/ship/:id', () => {
  let ship, shipId;

  const execute = () => request(app).delete(`/api/ship/${shipId}`);

  beforeEach(done => {
    shipId = new ObjectId();
    ship = {
      _id: shipId,
      name: 'exampleName',
      model: 'exampleModel',
      speed: 1,
      hyperdriveRating: 1
    }
    Ship.create(ship).then(() => done());
  });

  afterEach(done => {
    Ship.deleteMany().then(() => done());
  });

  it('should return 400 if provided id is not valid', done => {
    shipId = null;
    execute().expect(400).end(done);
  });

  it('should return 404 if there is no ship with provided id', done => {
    shipId = new ObjectId();
    execute().expect(404).end(done);
  });

  it('should delete ship and return 200 with the deleted ship', done => {
    execute().expect(200).end((err, res) => {
      expect(res.body.name).to.equal(ship.name);
      expect(res.body.speed).to.equal(ship.speed);
      Ship.countDocuments().then(count => {
        expect(count).to.equal(0);
        done();
      });
    });
  });

});

describe('GET /api/ship', () => {
  let ships;

  beforeEach(done => {
    ships = [{
      name: 'exampleName',
      model: 'exampleModel',
      speed: 1,
      hyperdriveRating: 1
    }, {
      name: 'anotherName',
      model: 'anotherModel',
      speed: 2,
      hyperdriveRating: 2
    }]
    Ship.insertMany(ships).then(() => done());
  });

  afterEach(done => {
    Ship.deleteMany().then(() => done());
  });

  it('should get all ships', done => {
    request(app).get(`/api/ship/`).expect(200).end((err, res) => {
      expect(res.body.length).to.equal(2);
      done();
    });
  })

});

describe('GET /api/ship/:id', () => {
  let ship, shipId;

  const execute = () => request(app).get(`/api/ship/${shipId}`);

  beforeEach(done => {
    shipId = new ObjectId();
    ship = {
      _id: shipId,
      name: 'exampleName',
      model: 'exampleModel',
      speed: 1,
      hyperdriveRating: 1
    }
    Ship.create(ship).then(() => done());
  });

  afterEach(done => {
    Ship.deleteMany().then(() => done());
  });

  it('should return 400 if provided id is not valid', done => {
    shipId = null;
    execute().expect(400).end(done);
  });

  it('should return 404 if there is no ship with provided id', done => {
    shipId = new ObjectId();
    execute().expect(404).end(done);
  });

  it('should return the correct ship', done => {
    execute().expect(200).end((err, res) => {
      expect(res.body.name).to.equal(ship.name);
      expect(res.body.speed).to.equal(ship.speed);
      done();
    });
  });
});