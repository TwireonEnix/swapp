const router = require('express').Router();
const shipCtrl = require('../controllers/ship');
const validator = require('../middleware/validator');
const validId = require('../middleware/validId');
const { validate } = require('../models/ship');

router.post('/', validator(validate), shipCtrl.createShip);
router.put('/:id', validId, validator(validate), shipCtrl.editShip);
router.delete('/:id', validId, shipCtrl.deleteShip);
router.get('/', shipCtrl.getShips);
router.get('/:id', validId, shipCtrl.getSingleShip);

module.exports = router;