const express = require('express');
const cors = require('../middleware/cors');
const error = require('../middleware/error');
const ship = require('../routes/ship');

module.exports = app => {
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));
  if (app.get('env') === 'development') app.use(require('morgan')('dev'));
  app.use(cors);
  app.get('/', (req, res) => res.send('Swapp backend online'));
  app.use('/api/ship', ship);
  app.use(error);
};