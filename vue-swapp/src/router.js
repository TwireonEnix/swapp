import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import store from './store';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [{
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/starships',
      name: 'FilmStarships',
      component: () => import('./views/MovieStarships'),
      beforeEnter: (to, from, next) => store.state.starship.starships.length ? next() : next('/')
    },
    {
      path: '/my-starships',
      name: 'MyStarships',
      component: () => import('./views/MyStarships')
    },
    {
      path: '/my-starships/:id',
      name: 'singleship',
      component: () => import('./views/StarshipDetails')
    },
    {
      path: '/404',
      name: 'notFound',
      component: () => import('./components/navigation/NotFound')
    },
    {
      path: '/error',
      name: 'error',
      component: () => import('./components/navigation/Error')
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
});