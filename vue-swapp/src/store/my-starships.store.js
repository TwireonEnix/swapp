import swapp from '../api/swapp.js';
import router from '../router';

export default {
  state: {
    myStarships: [],
    selectedStarship: null,
  },
  getters: {
    allMyShips: state => state.myStarships,
    oneStarship: state => state.selectedStarship
  },
  mutations: {
    setStarships: (state, ships) => state.myStarships = ships,
    setSelectedShip: (state, ship) => state.selectedStarship = ship
  },
  actions: {
    saveNewShip: (context, ship) => {
      swapp.saveNewShip(ship).then(() => {
        router.replace('/my-starships');
      }).catch(() => router.replace('/error'));
    },
    getShips: context => {
      context.commit('setLoading', true);
      swapp.fetchShips().then(ships => {
        context.commit('setStarships', ships.data);
        context.commit('setLoading', false);
      }).catch(() => router.replace('/error'));
    },
    getAShip: (context, shipId) => {
      context.commit('setLoading', true);
      swapp.fetchShips(shipId).then(ship => {
        context.commit('setSelectedShip', ship.data);
        context.commit('setLoading', false);
      }).catch(() => {
        router.replace('/404');
        context.commit('setLoading', false);
      });
    },
    deleteShip: (context, shipId) => {
      swapp.deleteShip(shipId).then(() => {
        context.commit('setSelectedShip', null);
        router.replace('/my-starships');
      }).catch(() => router.replace('/error'));
    }
  }
};