export default {
  state: {
    sidebar: null,
    dark: localStorage.getItem('dark'),
    loading: false
  },
  getters: {
    isDark: state => !!state.dark,
    isLoading: state => state.loading
  },
  mutations: {
    sidebar: (state, value) => state.sidebar = value,
    toggleSidebar: state => state.sidebar = !state.sidebar,
    toggleDark: (state, value) => state.dark = value,
    setLoading: (state, value) => state.loading = value
  },
  actions: {
    toggleDarkMode: (context) => {
      context.commit('toggleDark', !context.state.dark);
      context.state.dark ? localStorage.setItem('dark', true) : localStorage.removeItem('dark');
    }
  }
};