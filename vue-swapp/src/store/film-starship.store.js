import swapi from '../api/swapi';
import router from '../router';

export default {
  state: {
    starships: []
  },
  getters: {
    filmStarships: state => state.starships
  },
  mutations: {
    setFilmStarships: (state, starships) => state.starships = starships
  },
  actions: {
    getFilmStarships: (context, requests) => {
      context.commit('setLoading', true);
      requests = requests.map(url => url.match(/\d+/g)[0]);
      const promises = requests.map(request => swapi.fetchStarship(request));
      Promise.all(promises).then(starshipObjects => {
        const starships = starshipObjects.map(starship => starship.data);
        context.commit('setFilmStarships', starships);
        router.replace('/starships');
        context.commit('setLoading', false);
      }).catch(() => router.replace('/error'));
    }
  }
};