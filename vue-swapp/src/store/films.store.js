import swapi from '../api/swapi.js';
import router from '../router';

export default {
  state: {
    films: []
  },
  getters: {
    allFilms: state => state.films
  },
  mutations: {
    setFilms: (state, films) => state.films = films,
  },
  actions: {
    getFilms: (context) => {
      context.commit('setLoading', true);
      swapi.fetchFilms().then(response => {
        context.commit('setFilms', response.data.results);
        context.commit('setLoading', false);
      }).catch(() => router.replace('/error'));
    },
  }
};