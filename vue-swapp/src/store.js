import Vue from 'vue';
import Vuex from 'vuex';
import films from './store/films.store';
import nav from './store/nav.store';
import starship from './store/film-starship.store';
import myStarships from './store/my-starships.store';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    nav,
    films,
    starship,
    myStarships
  }
});