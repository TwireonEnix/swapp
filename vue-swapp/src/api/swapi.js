import axios from 'axios';
const api = axios.create({
  baseURL: 'https://swapi.co/api/'
});

export default {
  fetchFilms(id) {
    const movieId = id || '';
    return api.get(`films/${movieId}`);
  },
  fetchStarship: id => api.get(`starships/${id}/`)
};