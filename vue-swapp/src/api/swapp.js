import axios from 'axios';

const api = axios.create({
	baseURL: 'https://swapp-back.herokuapp.com/api/ship',
});

export default {
	fetchShips(id) {
		const shipId = id || '';
		return api.get(`/${shipId}`);
	},
	saveNewShip: shipData => api.post('/', shipData),
	deleteShip: id => api.delete(`/${id}`),
};
